import React, { Component } from 'react';

export class Services extends Component {
  render() {
    return (
      <section className="services-wrapper">
        <div className="services-wrapper-inner">
          <div className="services">
            <p>Услуги</p>
          </div>
          <div className="services-wrapper-for-Items">
            <div className="services-Item">
              <div className="services-Item-name green">Ручное бронирование</div>
              <div className="services-Item-howMany">11</div>
            </div>
            <div className="services-Item">
              <div className="services-Item-name blue1">Пакетные туры</div>
              <div className="services-Item-howMany">3</div>
            </div>
            <div className="services-Item">
              <div className="services-Item-name blue2">Отели</div>
              <div className="services-Item-howMany">1</div>
            </div>
          </div>
          <div className="services-total">
            <p>Всего</p>
            <span>15</span>
          </div>
        </div>
      </section>
    );
  }
}

export default Services;
