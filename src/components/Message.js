import React, { Component } from 'react';

export class Message extends Component {
  render() {
    const { name, text, date } = this.props;
    return (
      <div>
        <div className="messageName">
          <p>{name}</p>
          <span>{date}</span>
        </div>
        <div className="messageText">
          <div className="messageText-triangle"></div>

          <p>{text}</p>
        </div>
      </div>
    );
  }
}

export default Message;
