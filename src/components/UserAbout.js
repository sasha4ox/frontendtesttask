import React, { Component } from 'react';
import image from '../img/Image.png';
import Services from './Services';
import Messages from './Messages';

export class UserAbout extends Component {
  render() {
    return (
      <>
        <div className="UserAbout">
          <img src={image} alt="" />
          <div className="UserAbout_name_wrapper">
            <h3>Вероника Ростова</h3>
            <span>менеджер по продажам</span>
          </div>

          <p>Подберу для вас самые лучшие предложения. Мои услуги абсолютно бесплатны</p>
        </div>

        <Services />
        <Messages
          inputValue={this.props.inputValue}
          messagesData={this.props.messagesData}
          handleChange={this.props.handleChange}
          handleClick={this.props.handleClick}
        />
      </>
    );
  }
}

export default UserAbout;
