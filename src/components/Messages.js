import React, { Component } from 'react';
import message from '../img/message.png';
import heart from '../img/heart.png';
import Message from './Message';
export class Messages extends Component {
  render() {
    const messageData = this.props.messagesData.map((mess, index) => {
      return <Message key={index} date={mess.date} name={mess.name} text={mess.text} />;
    });
    return (
      <section className="messages-wrapper-global">
        <div className="messages-wrapper">
          <div className="messages-fullInfo">
            <p className="messages-fullInfo_latestCommit">Последние отзывы</p>
            <a href="!#">Все отзывы</a>

            <div className="messages-fullInfo_wrapper-for-like">
              <a href="!#">
                {' '}
                <img src={message} alt="" /> 131
              </a>
              <a href="!#">
                {' '}
                <img src={heart} alt="" /> 14
              </a>
            </div>
          </div>
        </div>
        {messageData}
        <section className="form-wrapper">
          <form action="">
            <textarea
              type="text"
              value={this.props.inputValue}
              onChange={this.props.handleChange}
            />
            <button type="submit" onClick={this.props.handleClick}>
              Написать консультанту
            </button>
          </form>
        </section>
      </section>
    );
  }
}

export default Messages;
