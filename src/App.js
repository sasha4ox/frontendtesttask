import React from 'react';
import UserAbout from './components/UserAbout';
import './App.scss';
import axios from 'axios';

class App extends React.Component {
  constructor() {
    super();
    this.state = {
      inputValue: '',
      messagesData: [
        {
          date: '13 октября 2011',
          name: 'Самуил',
          text: 'Привет, Верунь! ниче себе ты крутая. фотка класс!!!!  ',
        },
        {
          date: '13 октября 2011',
          name: 'Лилия Семёновна',
          text:
            'Вероника, здравствуйте! Есть такой вопрос: Особый вид куниц жизненно стабилизирует кинетический момент, это и есть всемирно известный центр огранки алмазов и торговли бриллиантами?  ',
        },
        {
          date: '14 октября 2011',
          name: 'Лилия Семёновна',
          text:
            'Вероника, здравствуйте! Есть такой вопрос: Особый вид куниц жизненно стабилизирует кинетический момент?  ',
        },
      ],
    };
  }
  componentDidMount() {
    document.addEventListener('keydown', this.keyfunc);
    var options = {
      year: 'numeric',
      month: 'long',
      day: 'numeric',
      timezone: 'UTC',
    };
    const fullDate = new Date();
    let myDate = fullDate.toLocaleString('ru', options);

    axios
      .get(`https://repetitora.net/api/JS/Tasks?widgetId=55888888&count=100`)
      .then(data => {
        return data.data;
      })
      .then(data => {
        const newArrayMessages = [];
        data.map(item => {
          return newArrayMessages.push({
            date: myDate,
            text: item.title,
            name: 'Anonymous',
          });
        });
        this.setState({
          messagesData: [...this.state.messagesData, ...newArrayMessages],
        });
      });
  }
  keyfunc = e => {
    if (e.keyCode === 13 && e.ctrlKey) {
      this.handleClick(e);
    } else if (e.keyCode === 13) {
      e.preventDefault();
    }
  };
  handleClick = e => {
    e.preventDefault();
    axios
      .post(`https://repetitora.net/api/JS/Tasks`, {
        widgetId: 55888888,
        title: this.state.inputValue,
      })
      .then(data => {
        if (data.status === 200) {
          axios
            .get(`https://repetitora.net/api/JS/Tasks?widgetId=55888888&count=100`)
            .then(data => {
              return data.data;
            })
            .then(data => {
              var options = {
                year: 'numeric',
                month: 'long',
                day: 'numeric',
                timezone: 'UTC',
              };
              const fullDate = new Date();
              let myDate = fullDate.toLocaleString('ru', options);

              const newArrayMessages = [
                {
                  date: myDate,
                  text: data[data.length - 1].title,
                  name: 'Anonimus',
                },
              ];
              this.setState({
                messagesData: [...this.state.messagesData, ...newArrayMessages],
                inputValue: '',
              });
            });
        }
      });
  };
  handleChange = e => {
    this.setState({
      inputValue: e.target.value,
    });
  };
  componentWillUnmount() {
    document.removeEventListener('keydown', this.keyfunc);
  }
  render() {
    return (
      <div className="App">
        <UserAbout
          inputValue={this.state.inputValue}
          messagesData={this.state.messagesData}
          handleChange={this.handleChange}
          handleClick={this.handleClick}
        />
      </div>
    );
  }
}

export default App;
